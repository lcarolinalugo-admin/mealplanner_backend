from authApp.models.foodinfo import Foods
from rest_framework import serializers

class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Foods
        fields = ['name','proteins','carbohydrate','fats','calories']