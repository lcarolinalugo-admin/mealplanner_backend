from authApp.models.foodinfo import Foods
from authApp.serializers.foodSerializer import FoodSerializer
from rest_framework import generics

class FoodListCreateView(generics.ListCreateAPIView):
    queryset = Foods.objects.all()
    serializer_class = FoodSerializer

class FoodRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Foods.objects.all()
    serializer_class = FoodSerializer






