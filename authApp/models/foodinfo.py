from django.db import models

class Foods(models.Model):
    cod = models.AutoField(primary_key=True)
    name=models.CharField(max_length = 50, unique=True)
    proteins = models.FloatField(default=0)
    carbohydrate = models.FloatField(default=0)
    fats = models.FloatField(default=0)
    calories= models.FloatField(default=0)
